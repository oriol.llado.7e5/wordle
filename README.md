# WORDLE



## Que es?

[Wordle](https://lapalabradeldia.com/) es un joc que te com a finalitat encertar una paraula. Consisteix en una interfície on tu poses la 
paraula de 5 lletres que creus que pot ser i el programa et detecta com estanposades les lletres <br>
- Si les lletres estan posades al lloc correccte les pinta de color 
![#A9DC76](https://placehold.co/15x15/A9DC76/A9DC76.png)**VERD** <br>
- Si esta a la paraula pero col·locada malament les pinta de color
![#FFD866](https://placehold.co/15x15/FFD866/FFD866.png)**GROC**<br>
- Si no esta a la paraula les pinta de color
![#939293](https://placehold.co/15x15/939293/939293.png)**GRIS**<br>
![title](fotos/exemple.png)

## Parts

- Titol, pixelart
- Menu de tria de si vols jugar o si vols sortir del joc
  - while on coprovem que juguem sempre que l'usuari no ens digui el contrari
- Introducció de la paraula
  - Input on llegim la paraula, la separem i determinem el color que ha de tenir cada caracter de la paraula
- Llista on ens mostra els colors i les lletres
  - Un array bidimensional on anem emagatzemant les lletres amb el seu color corresponent. Cada fila es un intent diferent

## Millores

- Inserir un teclat com el joc real
- Utilitzar el diccionari del [DIEC](https://dlc.iec.cat/) o de la [RAE](https://www.rae.es/)
- Control d'accents
- Crear interfície gràfica

## Coneixements adquirits

- Interació amb l'usuari i acceptar o descartar les seves entrades
- Treballar amb els colors de la consola
- Utilitzar llistes bidimensionals i recorre-les
